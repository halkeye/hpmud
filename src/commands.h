/* vim: set expandtab ts=4 sw=4:
 *
 * HARRY POTTER MUD - commands.h
 * Copyright (c) 2003 Gavin Mogan <halkeye@halkeye.net>
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (version 2) as published by the
 * Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * This notice must remain unaltered.
 *
 * $Id$
 */

#ifndef _COMMANDS_H_
#define _COMMANDS_H_

#include "player.h"
#include "defines.h"

// Hash map for commands
#include <map>

struct commandinfo {
    std::string name;
    int (*function)(Player&, std::string);
    int level; // permissons
};

typedef std::map<std::string, commandinfo *> command_hash_type;
 
void interpret(Player * p);

// Name: help
// Description: Shows a player the help menu (move to sql eventually? that might be kinda cool)
int cmd_help(Player& p, std::string args);
// name: quit
// Description: marks a player as ready to logoff
int cmd_quit(Player& p, std::string args);
// name:
// Description: look?
int cmd_look(Player& p, std::string args);
int cmd_who (Player& p, std::string args);
int cmd_goto(Player& p, std::string args);
int cmd_say(Player& p, std::string args);
// Move North
int cmd_north(Player& p, std::string args);
// Move South
int cmd_south(Player& p, std::string args);
// Move East
int cmd_east(Player& p, std::string args);
// Move West
int cmd_west(Player& p, std::string args);
// Save Player
int cmd_save(Player& p, std::string args);
// Show player its info
int cmd_score(Player& p, std::string args);
// List commands available to the user
int cmd_commands(Player& p, std::string args);
// Show the player thier inventory
int cmd_inventory(Player& p, std::string args);
// emote
int cmd_emote(Player& p, std::string args);
// version
int cmd_version(Player& p, std::string args);
// get an object (and put it into inventory)
int cmd_get(Player& p, std::string args);
// drop and object from inventory
int cmd_drop(Player& p, std::string args);
// Wear an object
int cmd_equip(Player& p, std::string args);

// shutdown
int cmd_shutdown(Player& p, std::string args);

// OLC COMMANDS
// edit room
int cmd_redit(Player& p, std::string args);

#endif
