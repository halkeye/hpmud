/* vim: set expandtab ts=4 sw=4:
 *
 * HARRY POTTER MUD - character.cpp
 * Copyright (c) 2003 Gavin Mogan <halkeye@halkeye.net>
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (version 2) as published by the
 * Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * This notice must remain unaltered.
 *
 * $Id$
 */

#include "character.h"
#include "helper.h"
#include "defines.h"
#include <string>

/**
 * Base class for player/npc
 */
Character::Character(void)
{
    ;
}

Character::~Character(void)
{
    ;
}


// GetName
// returns the player name in the format of "Firstname Lastname"
std::string Character::GetName(void)
{
    std::string name;
    name = UCFirst(firstname);
    name += " ";
    name += UCFirst(lastname);
    return name;
}

std::string Character::GetProperName(void)
{
    std::string name;
    if (gender == SEX_MALE) {
        name = "Mr. ";
    } else if (gender == SEX_FEMALE) {
        name = "Ms. ";
    } else {
        name = "error ";
    }
    name += UCFirst(lastname);

    return name;
}

bool Character::IsName(std::string name)
{
    if (!ToLower(firstname).compare(name) || !ToLower(lastname).compare(name))
        return true;
    return false;
}
