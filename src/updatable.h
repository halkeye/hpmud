/* vim: set expandtab ts=4 sw=4:
 *
 * HARRY POTTER MUD - updatable.h
 * Copyright (c) 2003 Gavin Mogan <halkeye@halkeye.net>
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (version 2) as published by the
 * Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * This notice must remain unaltered.
 *
 * $Id$
 */

#ifndef _UPDATABLE_H_
#define _UPDATABLE_H_

/**
 * Interface for Updateable Objects (doing things based on time count)
 *
 * @author Gavin Mogan <halkeye@halkeye.net>
 * @version $Id$
 */

class Updatable
{
private:
    int tickcount;
public:
    /**
     * Update tick count, and do jobs accordingly
     */
    virtual bool Update(void) = 0;
};

#endif

