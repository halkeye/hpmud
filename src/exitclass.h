/* vim: set expandtab ts=4 sw=4:
 *
 * HARRY POTTER MUD - exitclass.h
 * Copyright (c) 2003 Gavin Mogan <halkeye@halkeye.net>
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (version 2) as published by the
 * Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * This notice must remain unaltered.
 *
 * $Id$
 */

#ifndef _EXITCLASS_H_
#define _EXITCLASS_H_

#include <string>
#include <fstream>
#include "roomclass.h"

class RoomClass;
class ExitClass
{
public:
    ExitClass(void);
    ExitClass(int number    );
    ~ExitClass(void);

    std::string GetRoomName(void);
    std::string GetDirection(void);
    void Save(std::ofstream & out);
public: 
    // exitnum
    int exitid;
    // Which direction is exit
    exit_types direction;
    // exit description? for look north? maybe?
    std::string description;
    // Where does it go?
    RoomClass * to;
    // where does it come from
    RoomClass * from;
};
#endif
