/* vim: set expandtab ts=4 sw=4:
 *
 * HARRY POTTER MUD - objectclass.cpp
 * Copyright (c) 2003 Gavin Mogan <halkeye@halkeye.net>
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (version 2) as published by the
 * Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * This notice must remain unaltered.
 *
 * $Id$
 */

#include "objectclass.h"
#include "roomclass.h"

ObjectClass::ObjectClass(int num)
{
    objnum = num;
    inroom = NULL;
    age    = 100; // how many seconds should it start with?
}

ObjectClass::~ObjectClass(void)
{
    if (inroom)
    {
        inroom->RemFromRoom(this);
        inroom = NULL;
    }

}

std::string ObjectClass::LookData(void)
{
    return GetName() + NEWLINE;
}

void ObjectClass::SetRoom(RoomClass * room)
{
    inroom = room;
    return;
}

void ObjectClass::SetWearLoc(int loc)
{
    if (loc < 0)
        loc = WEAR_NONE; // don't change if location doesn't exist
    if (loc > MAX_WEARLOC)
        loc = WEAR_NONE; // don't change if location doesn't exist
    wearloc = loc;
    return;
}

int ObjectClass::GetWearLoc(void)
{
    return wearloc;
}
