/* vim: set expandtab ts=4 sw=4:
 *
 * HARRY POTTER MUD - hpmud.cpp
 * Copyright (c) 2003 Gavin Mogan <halkeye@halkeye.net>
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (version 2) as published by the
 * Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * This notice must remain unaltered.
 *
 * $Id$
 */

#include <string>
#include <map>
#include <utility>
#include <cstdio>
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <algorithm>

#include "logger.h"

#include <unistd.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <sys/types.h>
#include <sys/wait.h>

#include <arpa/telnet.h>

#include "defines.h"
#include "player.h"
#include "hpmud.h"
#include "commands.h"
#include "helper.h"
#include "roomclass.h"
#include "objectclass.h"
#include "area.h"

#include "simplexml.h"

const unsigned char hpmud::echo_off_str[] = {IAC, WILL, TELOPT_ECHO, '\0'};
const unsigned char hpmud::echo_on_str[] = {IAC, WONT, TELOPT_ECHO, '\0'};
RoomClass * hpmud::testroom = NULL;

hpmud::hpmud(void)
{
    b_shutdown = false;
}

hpmud::~hpmud(void)
{
}

// Code that loops repeatedly (should be fork(), but i don't know how todo shared memory yet)
void hpmud::GameLoop(void)
{
    int max_fds;
    struct timeval tv;
    fd_set read_fds;
    fd_set err_fds;
    time_t tTime = 0;

    FD_ZERO(&read_fds);
    FD_ZERO(&err_fds);
    FD_SET(control,&read_fds);
    max_fds = control+1;
    
    do {
        memcpy((char*)&read_fds,(char*)&master_fdset,sizeof(read_fds));
        memcpy((char*)&err_fds,(char*)&master_fdset,sizeof(read_fds));

        tv.tv_sec = 1;
        tv.tv_usec = 0;
        
        /* Update pulse called every second */
        if ( (time(0) - tTime) >= 1 /* second */ ) {
            UpdateHandler();
            tTime = time(0);
        }

        if (select (max_fds, &read_fds, (fd_set*)0,&err_fds, &tv) < 0) {
            perror("select");
            continue;
        }

        if ( FD_ISSET( control, &err_fds ) ) {
/*            char message[40];
            sprintf(message, "Exception raise on controlling descriptor %d", control);
            std::string temp(message);
            game.log( temp );*/
            Logger lj;
            lj << "Exception raise on controlling descriptor " << control << NEWLINE;
            FD_CLR( control, &read_fds );
        }
        /* if control is set on readfds accept new client */
        else if (FD_ISSET(control,&read_fds))
        {
            int new_fd;
            struct sockaddr_in stSa;
            unsigned int iSaSize=sizeof(stSa);
            
            if ((new_fd = accept(control, (struct sockaddr*)&stSa,&iSaSize))>=0) {
                addConnection(new_fd);
            }
            FD_CLR(control, &read_fds);
            max_fds = UMAX(max_fds, new_fd+1);
        }       
        if (!connections.empty())
        {
            Player * p;
            connection_map::iterator i;
            int socketd;

            for( i = connections.begin(); i != connections.end(); ++i)
            {
                p = &(i->second.GetPlayer());
                if (p == NULL) 
                {
                    /* To Date this has NEVER happened */
                    game.log("fatal error?");
                    continue;
                }
                socketd = i->second.GetSocket();
                if ( FD_ISSET( socketd, &err_fds) )
                {
                    fprintf( stderr, "Exception raise on controlling descriptor %d", socketd );
                    FD_CLR( socketd, &read_fds );
                }
                if (FD_ISSET(socketd, &read_fds) && !recv(p))
                {
                    CloseSocket(p);
                }
            }
        }
    } while (!IsShutdown());
    close(control);
}

int hpmud::Init(int port)
{
    struct sockaddr_in addr;
    int yes=1;

    if ((control = socket(AF_INET, SOCK_STREAM, 0)) == -1)
    {
        perror("socket");
        exit(1);
    }
    
    if (setsockopt(control, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1)
    {
        perror("setsockopt");
        exit(1);
    }
    
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = htonl(INADDR_ANY);
    memset(&(addr.sin_zero), '\0', 8);
    
    if (bind(control, (struct sockaddr *)&addr, sizeof(struct sockaddr)) == -1)
    {
        perror("bind");
        exit(1);
    }
    
    if (listen(control, SOMAXCONN) == -1)
    {
        perror("listen");
        exit(1);
    }
    
    FD_ZERO(&master_fdset);
    FD_SET(control, &master_fdset);
    return control;
}

bool hpmud::CloseSocket(Player * p)
{
    if (!p) return false; // some sorta error

    close(p->GetConnection().GetSocket());
    FD_CLR(p->GetConnection().GetSocket(), &master_fdset);

    if (!players.empty())
    {
        player_map::iterator i = players.find(p->GetFirstName());
        if (i != players.end())
            players.erase(i);
        else
            std::cerr << "can't find player" << std::endl;
    }

    if (!connections.empty())
     {
        connection_map::iterator i = connections.find(p->GetConnection().GetSocket());
        if (i != connections.end())
            connections.erase(i);
     }

    delete p;   
    return true; // i can't see any case where it might fail
}

bool hpmud::addConnection(unsigned int socketd)
{
    Player * player = new Player(socketd);
    
    FD_SET(socketd, &master_fdset); 
    connections.insert(std::make_pair(socketd,player->GetConnection()));

    /*helpdata * hd = game.connection->getHelp("greeting", false);
    player->SendToPlayer(hd->data);
    delete hd;*/

    player->GetConnection() << hpmud::echo_on_str;
    player->Prompt();
    
    Logger lg;
    lg << "New connection from " <<  player->GetConnection().GetIp() << NEWLINE;

    return true; /* connetion created successfuly */
}

bool hpmud::recv(Player * p)
{
    int len_remaining; /* how much more do we have to get? */
    char chs[1];
    int ch;
    do
    {
        len_remaining = read(p->GetConnection().GetSocket(),chs,1);
        if (len_remaining <= 0)
        {
            return false;
        }
        /* FIXME - hack */
        ch = chs[0];
        
        /* End of line */
        if (ch == '\n' || ch == '\r')
        {
            if (p->GetConnection().inputbuffer.length() > 0)
            {
                p->SetArguments(p->GetConnection().inputbuffer);
                p->GetConnection().inputbuffer = "";
                interpret(p);
                return true;
            }
        } else {
            /* Don't store non-printable characters */
            if (isprint(ch)) {
                // trim spaces at the begging
                if (isalnum(ch) || !p->GetConnection().inputbuffer.empty())
                    p->GetConnection().inputbuffer += ch;
            } else 
                // backspace.. there has to be a better escape code or something
                // only do it when there is room to delete.
            if (ch == 8 && p->GetConnection().inputbuffer.length() > 0 ) { 
                p->GetConnection().inputbuffer.resize(p->GetConnection().inputbuffer.length()-1);
            }
        }
    } while (len_remaining > 1);
    return true;
}

Player * hpmud::FindPlayer(std::string name)
{
    player_map::iterator i = players.find(name);
    if (i != players.end())
        return (i->second);
    return NULL;
}


// logs to file/stdout/etc... logtype says which file or even console it goes to
void hpmud::log(std::string message)
{
    std::cout << "[LOG] " << message << std::endl;
}

int hpmud::InitGame(void)
{
    /* Load Rooms from file (or sql) */
    hpmud::testroom = new RoomClass(0);
    
    std::ifstream in("../areas/areas.lst");
    std::string str = "";
    std::string temp;
    
    getline(in,temp);
    while ( in ) {
        str += temp;
        getline(in,temp);
    }
    in.close();
    
    simplexml *root = new simplexml(str.c_str());
    for (int a = 0; a < root->number_of_children(); ++a)
    {
        Area * area = new Area();
        area->load(root->child(a)->value());
        areas.push_back(area);
    } 
    delete root;
    /*
    RoomClass * room;
    int count = 0;
    if ((results = game.connection->GetQuery("SELECT COUNT(*) FROM rooms")))
    {
        row = mysql_fetch_row(results);
        count = atoi(row[0]);
        mysql_free_result(results);
    }
    for (int x = 0; x < count ; ++x)
    {
        char sql[300];
        sprintf(sql, "SELECT r.roomid, r.roomname, rd.description FROM rooms r, room_desc rd WHERE r.roomid = rd.roomid LIMIT %d,1", x);
        results = game.connection->GetQuery(sql);
        if (results)
        {
            row = mysql_fetch_row(results);
            std::string name = (char *) row[1];
            std::string desc = (char *) row[2];
            std::string num = (char *) row[0];
            room = temp->CreateRoom(atoi(num.c_str()));
            room->SetName(name);
            room->SetDesc(desc);
            mysql_free_result(results);
        }
    }
    // Now do exits
    if ((results = game.connection->GetQuery("SELECT COUNT(*) FROM exits")))
    {
        row = mysql_fetch_row(results);
        count = atoi(row[0]);
        mysql_free_result(results);
    }
    for (int x = 0; x < count ; ++x)
    {
        char sql[300];
        sprintf(sql, "SELECT roomfrom, roomto, direction,exitid FROM exits WHERE 1 LIMIT %d,1", x);

        results = game.connection->GetQuery(sql);
        if (results)
        {
            row = mysql_fetch_row(results);
            int room1num = atoi( (char *) row[0]);
            int room2num = atoi( (char *) row[1]);
            char * direction = (char *) row[2];
            if (!strcmp(direction, "north"))
                FindRoom(room1num)->CreateExit(FindRoom(room2num), EXIT_NORTH, atoi( (char*) row[3]));
            else if (!strcmp(direction, "south"))
                FindRoom(room1num)->CreateExit(FindRoom(room2num), EXIT_SOUTH, atoi( (char*) row[3]));
            else if (!strcmp(direction, "east"))
                FindRoom(room1num)->CreateExit(FindRoom(room2num), EXIT_EAST, atoi( (char*) row[3]));
            else if (!strcmp(direction, "west"))
                FindRoom(room1num)->CreateExit(FindRoom(room2num), EXIT_WEST, atoi( (char*) row[3]));

            mysql_free_result(results);
        }
    }
    */
    return 1;
}

void hpmud::InitObjs(void)
{
    /*ObjectClass * obj;
    MYSQL_ROW row;
    MYSQL_RES * results;
    int count = 0;

    if ((results = game.connection->GetQuery("SELECT COUNT(*) FROM objects")))
    {
        row = mysql_fetch_row(results);
        count = atoi(row[0]);
        mysql_free_result(results);
    }
    for (int x = 0; x < count ; ++x)
    {
        char sql[300];
        sprintf(sql, "SELECT objname, inroom, objid, wearloc FROM objects WHERE 1 LIMIT %d,1", x);
        results = game.connection->GetQuery(sql);
        if (results)
        {
            row = mysql_fetch_row(results);
            int num = atoi((char *) row[2]);
            obj = new ObjectClass(num);
            obj->SetName((char *)row[0]);
            obj->SetWearLoc(atoi((char *) row[3]));
            objects.insert(std::make_pair(obj->GetName(),obj));
            FindRoom(atoi((char *) row[1]))->AddToRoom(obj);
            mysql_free_result(results);
        }
    }*/
}

void hpmud::InitCommands(void)
{
    commandinfo * info;
    std::string temp;

    info = new commandinfo;
    info->function = &cmd_commands;
    info->name     = "commands";
    info->level    = USER_ALL;
    command_hash.insert(std::make_pair(info->name,info));

    info = new commandinfo;
    info->function  = &cmd_east;
    info->name      = "east";
    info->level = USER_ALL;
    command_hash.insert(std::make_pair(info->name,info));
    temp = "e";
    command_hash.insert(std::make_pair(temp,info));

    info = new commandinfo;
    info->function  = &cmd_goto;
    info->name  = "goto";
    info->level = USER_GOD|USER_TEACHER;
    command_hash.insert(std::make_pair(info->name,info));

    info = new commandinfo;
    info->function  = &cmd_help;
    info->name  = "help";
    info->level = USER_ALL;
    command_hash.insert(std::make_pair(info->name,info));

    info = new commandinfo;
    info->function  = &cmd_look;
    info->name  = "look";
    info->level = USER_ALL;
    command_hash.insert(std::make_pair(info->name,info));
    temp = "l";
    command_hash.insert(std::make_pair(temp,info));
    
    info = new commandinfo;
    info->function  = &cmd_north;
    info->name  = "north";
    info->level     = USER_ALL;
    command_hash.insert(std::make_pair(info->name,info));
    temp = "n";
    command_hash.insert(std::make_pair(temp,info));

    info = new commandinfo;
    info->function  = &cmd_quit;
    info->name  = "quit";
    info->level     = USER_ALL;
    command_hash.insert(std::make_pair(info->name,info));

    info = new commandinfo;
    info->function  = &cmd_south;
    info->name  = "south";
    info->level     = USER_ALL;
    command_hash.insert(std::make_pair(info->name,info));
    temp = "s";
    command_hash.insert(std::make_pair(temp,info));

    info = new commandinfo;
    info->function  = &cmd_score;
    info->name  = "score";
    info->level     = USER_ALL;
    command_hash.insert(std::make_pair(info->name,info));

    info = new commandinfo;
    info->function  = &cmd_say;
    info->name  = "say";
    info->level     = USER_ALL;
    command_hash.insert(std::make_pair(info->name,info));

    info = new commandinfo;
    info->function  = &cmd_west;
    info->name  = "west";
    info->level     = USER_ALL;
    command_hash.insert(std::make_pair(info->name,info));
    temp = "w";
    command_hash.insert(std::make_pair(temp,info));

    info = new commandinfo;
    info->function  = &cmd_who;
    info->name  = "who";
    info->level     = USER_ALL;
    command_hash.insert(std::make_pair(info->name,info));

    info = new commandinfo;
    info->function  = &cmd_inventory;
    info->name  = "inventory";
    info->level     = USER_ALL;
    command_hash.insert(std::make_pair(info->name,info));

    info = new commandinfo;
    info->function  = &cmd_emote;
    info->name  = "emote";
    info->level     = USER_ALL;
    command_hash.insert(std::make_pair(info->name,info));

    info = new commandinfo;
    info->function  = &cmd_version;
    info->name  = "version";
    info->level     = USER_ALL;
    command_hash.insert(std::make_pair(info->name,info));

    info = new commandinfo;
    info->function  = &cmd_get;
    info->name  = "get";
    info->level     = USER_ALL;
    command_hash.insert(std::make_pair(info->name,info));

    info = new commandinfo;
    info->function  = &cmd_drop;
    info->name  = "drop";
    info->level     = USER_ALL;
    command_hash.insert(std::make_pair(info->name,info));

    info = new commandinfo;
    info->function  = &cmd_save;
    info->name  = "save";
    info->level     = USER_ALL;
    command_hash.insert(std::make_pair(info->name,info));

    info = new commandinfo;
    info->function  = &cmd_equip;
    info->name      = "equip";
    info->level     = USER_ALL;
    command_hash.insert(std::make_pair(info->name,info));
    temp = "wear";
    command_hash.insert(std::make_pair(temp,info));

    info = new commandinfo;
    info->function  = &cmd_shutdown;
    info->name  = "shutdown";
    info->level     = USER_GOD;
    command_hash.insert(std::make_pair(info->name,info));

    info = new commandinfo;
    info->function  = &cmd_redit;
    info->name  = "redit";
    info->level     = USER_GOD|USER_TEACHER;
    command_hash.insert(std::make_pair(info->name,info));

}

int hpmud::UpdateHandler(void)
{
    //log("update");
    // for all mob
    // dojob();
    // depending on type of mob, specs perhaps, it does certain things
    
    //return 1;

    /*if (!objects.empty())
    {
        object_map::iterator i_obj;
        ObjectClass * obj;
        RoomClass * room;
        
        for( i_obj = objects.begin(); i_obj != objects.end(); ++i_obj)
        {
            obj = i_obj->second;
            if (obj)
            {
                if (--obj->age == 0)
                {
                    obj->age = 100;
                    if ((room = obj->GetRoom()) != NULL)
                        room->SendToChar(obj->GetName() + " falls apart, but suddently something new appears in its place." NEWLINE);
                    // testing update handler
                }
            }
        }
    }*/
    
    /*if (!rooms.empty())
    {
        room_map::iterator i;
        for (i = rooms.begin() ; i != rooms.end(); ++i)
        {
            ; // for all characters in the room
            // Update();
            // Update then updates all the rooms
        }
    }*/

    return 1;
}


RoomClass * hpmud::FindRoom(int rnum)
{
    if (!areas.empty()) 
    {
        area_list::iterator i_area;
        RoomClass * p_room = NULL;
        Area * p_area;
        for( i_area = areas.begin(); i_area != areas.end(); ++i_area)
        {
            p_area = (*i_area);
            p_room = p_area->FindRoom(rnum);
            if (p_room) return p_room;
        }
    }
    
    return NULL;
}
