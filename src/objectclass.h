/* vim: set expandtab ts=4 sw=4:
 *
 * HARRY POTTER MUD - objectclass.h
 * Copyright (c) 2003 Gavin Mogan <halkeye@halkeye.net>
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (version 2) as published by the
 * Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * This notice must remain unaltered.
 *
 * $Id$
 */

#ifndef _OBJECTCLASS_H_
#define _OBJECTCLASS_H_

#include <string>

class RoomClass;

class ObjectClass
{
protected:
    // name of the object
    std::string name;
    // indetification number
    int objnum;
    // what room is it in?
    RoomClass * inroom;
    // wearable location
    int wearloc;
    
public:
    ObjectClass(int num);
    ~ObjectClass(void);

    // Returns the name of the object
    std::string GetName(void) { return name; }
    // Gets the name of the object
    void SetName(std::string newname) { name = newname; }
    // Data for object
    std::string LookData(void);
    // Where are we?
    RoomClass * GetRoom(void) { return inroom; }
    // Set room location
    void SetRoom(RoomClass * room);
    // Set Wearable Locations
    void SetWearLoc(int loc);
    // Get Wearable location
    int GetWearLoc(void);
    // Is the object wearable?
    bool IsWearable(void) { if (wearloc != 0 ) return true; return false; }
    // age
    unsigned short int age;
};

#endif
