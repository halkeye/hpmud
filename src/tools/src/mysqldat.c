#include <stdio.h>

int main(int argc, char ** argv) {
	char filename[25];
	char username[25];
	char password[25];
	char hostname[50];
	char dbname[25];

	char temp[50];
	FILE * fp;

	printf("Filename to write to: ");
	scanf("%s", filename);
	printf("Filename %s\n", filename);
	// open data dir
	fp = fopen(filename, "wb");
	//
	// ask for username
	printf("Username: ");
	scanf("%s", username);
	// password
	printf("Password: \n");
	strcpy(password, "");
//	scanf("%s", password);
	// hostname
	printf("Hostname: ");
	scanf("%s",hostname);
	// dbname
	printf("Database Name: ");
	scanf("%s", dbname);
	// fwrite to disk
	fwrite(username, sizeof(char), 25, fp);
	fwrite(password, sizeof(char), 25, fp);
	fwrite(hostname, sizeof(char), 50, fp);
	fwrite(dbname, sizeof(char), 25, fp);
	// fclose
	fclose(fp);
	return 1;
}
