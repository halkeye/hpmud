/* vim: set expandtab ts=4 sw=4:
 *
 * Heroes Paradise MUD - area.cpp
 * Copyright (c) 2003 Gavin Mogan <halkeye@halkeye.net>
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (version 2) as published by the
 * Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * This notice must remain unaltered.
 */

#include <list>
#include "roomclass.h"
#include "defines.h"
#include "area.h"
#include "simplexml.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

Area::Area()
{
    strcpy(filename,"../areas/default.area");
    strcpy(name, "Default Area");
}

bool Area::Save(void)
{
    std::ofstream out(filename);
    out << "<area name=\"" << name << "\">\n";
    if (!rooms.empty()) 
    {
        room_map::iterator i_area;
        RoomClass * p_room;
        for( i_area = rooms.begin(); i_area != rooms.end(); ++i_area)
        {
            (*i_area->second).Save(out);
            // for all rooms
            // room->save(out);
        }
    }
    out << "</area>\n";
    out.close();
    return true;
}

bool Area::load(const char * name)
{
    std::ifstream in(name);
    std::string str = "";
    std::string temp;
    if (!in) return false;
    strcpy(filename,name);
    
    getline(in,temp);
    while ( in ) {
        str += temp;
        getline(in,temp);
    }
    in.close();
    
    simplexml *root = new simplexml(str.c_str());
    for (int a = 0; a < root->number_of_children(); ++a)
    {
        simplexml * child = root->child(a);
        // Each should be a room node
        RoomClass * room = new RoomClass(0);
        room->SetRnum(atoi(child->child("number")->value()));
        room->SetDesc(child->child("desc")->value());
        room->SetName(child->child("name")->value());
        simplexml * exits = child->child("exists");
        if (exits) {
            // we have exits, pass it to the exits load handler
        }
        rooms.insert(std::make_pair(room->GetRnum(), room));
    } 
    delete root; 
}

RoomClass * Area::FindRoom(int rnum)
{
    room_map::iterator i = rooms.find(rnum);
    if (i != rooms.end())
        return i->second;

    return NULL;    
}

