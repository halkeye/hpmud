/* vim: set expandtab ts=4 sw=4:
 *
 * HARRY POTTER MUD - exitclass.cpp
 * Copyright (c) 2003 Gavin Mogan <halkeye@halkeye.net>
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (version 2) as published by the
 * Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * This notice must remain unaltered.
 *
 * $Id$
 */

#include "exitclass.h"
#include "roomclass.h"
#include "hpmud.h"
#include <cstdio>
#include <fstream>

ExitClass::ExitClass(void)
{
    exitid = 0;
}

ExitClass::ExitClass(int number)
{
    exitid = number;
}

ExitClass::~ExitClass(void)
{
}

std::string ExitClass::GetRoomName(void) 
{ 
    if (to) 
    { 
        return to->GetName();
    } 
    return ""; 
}

std::string ExitClass::GetDirection(void)
{
    switch (direction)
    {
    case EXIT_NORTH:
        return "North";
    case EXIT_SOUTH:
        return "South";
    case EXIT_EAST:
        return "East";
    case EXIT_WEST:
        return "West";
    default:
        return "";
    }
    
}

void ExitClass::Save(std::ofstream & out)
{

    out << "    <exit>\n";
    out << "      <direction>North</direction>\n";
    out << "      <to>2</to>\n";
    out << "    </exit>\n";

    return;
}
