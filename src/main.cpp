/* vim: set expandtab ts=4 sw=4:
 *
 * HARRY POTTER MUD - main.cpp
 * Copyright (c) 2003 Gavin Mogan <halkeye@halkeye.net>
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (version 2) as published by the
 * Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * This notice must remain unaltered.
 *
 * $Id$
 */

#include "hpmud.h"
#include "logger.h"
#include <cstdio>
#include <stdlib.h>

hpmud game;

const std::string version = "$Id$";

int main(int argc, char * argv[])
{
    Logger lg;
    
    if (argc > 1)
    {
        game.Init(atoi(argv[1]));
    } else {
        game.Init();
    }
    game.InitGame();
    game.InitCommands();
    game.InitObjs();
    lg << "Game Started Up: " << version << NEWLINE;
    game.GameLoop();
    srand( (unsigned)time( NULL ) );
    
    return 0;
}
