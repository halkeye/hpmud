/* vim: set expandtab ts=4 sw=4:
 *
 * HARRY POTTER MUD - roomclass.cpp
 * Copyright (c) 2003 Gavin Mogan <halkeye@halkeye.net>
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (version 2) as published by the
 * Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * This notice must remain unaltered.
 *
 * $Id$
 */

#include "roomclass.h"
#include "exitclass.h"
#include "player.h"
#include "hpmud.h"
#include "objectclass.h"
#include "area.h"
#include <cstdio>
#include <sstream>
#include <fstream>

RoomClass::RoomClass(int num)
{
    rnum = num;
    name = "Default Room";
    description = "Blank. Fix Me";
}

RoomClass::~RoomClass(void)
{
    /* check to see if room is empty */
    // when are we deleting anyways?
}

int RoomClass::SendToChar(std::string message, Player * nosend)
{
    player_map::iterator i;
    Player * p;
    if (!players.empty())
    {
        for( i = players.begin(); i != players.end(); ++i)
        {
            p = i->second;
            if (p != NULL && p != nosend && p->GetState() == STATE_PLAYING)
                p->SendToPlayer(message);
        }
        return 1;
    }
    return 0;
}
// merge with other file

std::string RoomClass::LookData(void)
{
    //unsigned int short count;
    std::stringstream output;

    output << "@W@" << name;
    //if (p->GetLevel >= LEVEL_TEACHER)
    output << " [" << rnum << "]" << NEWLINE;
    /*for (count = 0; count < name.length(); count++)
        output << "=";*/

    output << "@c@" NEWLINE;
    output << description + NEWLINE;

    output << NEWLINE;
    output << "Exits";
    output << NEWLINE;
    output << "=====";
    output << NEWLINE;
    
    if (!exits.empty()) 
    {
        exit_list::iterator i_exit;
        ExitClass * p_exit;
        for( i_exit = exits.begin(); i_exit != exits.end(); ++i_exit)
        {
            p_exit = (*i_exit);
            if (p_exit)
            {
                output << p_exit->GetDirection();
                output << " - ";
                output << p_exit->GetRoomName();
                output << NEWLINE;
            }
        }
    } else {
        output << "None";
        output << NEWLINE;
    }

    if (!objects.empty()) 
    {
        object_map::iterator i_obj;
        ObjectClass * obj;
        for( i_obj = objects.begin(); i_obj != objects.end(); ++i_obj)
        {
            obj = i_obj->second;
            if (obj)
                output << "@G@" + obj->GetName() + " is on the floor" NEWLINE;
        }
    }

    return output.str();
}

std::string RoomClass::LookData(Player * p)
{
    std::string ret = "@P@";

    if (!players.empty()) 
    {
        player_map::iterator i;
        Player * p_player;
        ret += NEWLINE;
        for( i = players.begin(); i != players.end(); ++i)
        {
            p_player = i->second;
            if (p_player && p_player->GetState() == STATE_PLAYING && p_player != p)
            {
                ret += p_player->GetName();
                ret += " is standing here.";
                ret += NEWLINE;
            }
        }
    } 
    // List of players
    return ret;
}

// Put Character in room
void RoomClass::AddToRoom(Player * p)
{
    // add player to player list
    players.insert(std::make_pair(p->GetFirstName(), p));
}

// Take Character from room
void RoomClass::RemFromRoom(Player * p)
{
    player_map::iterator i;
    if (!players.empty()) 
    {
        i = players.find(p->GetFirstName());
        if (i != players.end())
            players.erase(i);
    }
}


// Put Object in room
void RoomClass::AddToRoom(ObjectClass * o)
{
    // add player to player list
    o->SetRoom(this);
    objects.insert(std::make_pair(o->GetName(), o));
}

// Take Object from room
void RoomClass::RemFromRoom(ObjectClass * o)
{
    object_map::iterator i;
    if (!objects.empty()) 
    {
        i = objects.find(o->GetName());
        if (i != objects.end())
        {
            i->second->SetRoom(NULL);
            objects.erase(i);
        }
    }
}

RoomClass * RoomClass::CreateRoom(int num)
{
    RoomClass * r = new RoomClass(num);
    if (inarea) 
      inarea->rooms.insert(std::make_pair(num, r));
    return r;
}

ExitClass * RoomClass::CreateExit(RoomClass * room, exit_types dir, int exitnum)
{
    ExitClass * e1 = new ExitClass(exitnum);
    
    e1->direction = dir;
    e1->to = room;
    e1->from = this;
    exits.insert(exits.begin(), e1);
    return e1;
}
// finds and returns the exit in said direction
RoomClass * RoomClass::FindExit(exit_types dir)
{
    exit_list::iterator i;
    if (!exits.empty()) 
    {
        ExitClass * e;
        for( i = exits.begin(); i != exits.end(); ++i)
        {
            e = (*i);
            // is this the exit we want?
            if (e->direction == dir)
                return e->to;
        }
    }
    return NULL;
}

// There arn't going to be that many players in a room right?
// no need for a hashmap right now (although it could be added to AddToRoom
Player * RoomClass::FindPlayer(std::string player_name)
{
    player_map::iterator i;

    i = players.find(player_name);
    if (i != players.end() )
        return i->second;
    for( i = players.begin(); i != players.end(); ++i)
    {
        Player * p = i->second;
        if ( p && ( p->IsName(player_name) || str_prefix(player_name,p->GetFirstName())  )) {
            return i->second;
        }
    }
    
    return NULL;
}

ObjectClass * RoomClass::FindObject(std::string object_name)
{
    object_map::iterator i;
    i = objects.find(object_name);
    if (i != objects.end() )
        return i->second;

    // we didn't find what we wanted in the player's inventory
    // so try word by word search
    for( i = objects.begin(); i != objects.end(); ++i)
    {
        ObjectClass * obj = i->second;
        if ( str_prefix(object_name, obj->GetName()) )
        {
            return obj;
        }
    }
    return NULL;
}


bool RoomClass::Save(std::ofstream &out)
{
    // Filename = ../areas/default.area
    // yea.. thats clean?
    // ofstream out("../areas/default.area");
    out << "<room>\n";
    out << "  <number>" << rnum << "</number>\n";
    out << "  <name>" << name << "</name>\n";
    out << "  <desc>" << description << "</desc>\n";
    out << "  <exits>\n";
    if (!exits.empty()) 
    {
        for (exit_list::iterator i_exit = exits.begin(); i_exit != exits.end(); ++i_exit)
            (*i_exit)->Save(out);
    }
/*    out << "    <exit>\n";
    out << "      <direction>North</direction>\n";
    out << "      <to>2</to>\n";
    out << "    </exit>\n";*/
    out << "  </exits>\n";
    out << "</room>\n";

    return true;
}

bool RoomClass::Update(void)
{
    /* For Each Updatable Object? */
    return true;
}
