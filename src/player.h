/* vim: set expandtab ts=4 sw=4:
 *
 * HARRY POTTER MUD - player.h
 * Copyright (c) 2003 Gavin Mogan <halkeye@halkeye.net>
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (version 2) as published by the
 * Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * This notice must remain unaltered.
 *
 * $Id$
 */

#ifndef _PLAYER_H_
#define _PLAYER_H_

#include <string>
#include <list>
#include "character.h"
#include "connection.h"
#include "helper.h"
#include "roomclass.h"
#include "defines.h"
#include "updatable.h"

class RoomClass;

class Player : 
    public Character, 
    public Updatable
{
private:
    // connection info
    Connection* connection;
    // what room is the player in?
    RoomClass * inroom;
    // player password
    std::string password;
    // player level - command wise
    int level; // TODO : cmdlevel
    // What state is the connection in
    conn_state  state;
    // wheee
    std::string arguments;
    // playerid (sql purposes only)
    short int playerid;
    // is there anything in its inventory?
    object_vector inventory;
    // Equipment
    object_vector equiped;

    std::string outbuf;

    // Have we loaded yet?
    bool loaded;
public:
    // Constructor
    // Creates player with default settings
    Player(unsigned int socketd);

    // Desctructor
    // Destroys a Player, and free its memory.. should it save first?
    ~Player(void);

    // Function:  SendToPlayer
    // Arguments: string - text to send to the player
    // returns:   true if succesful, false if not
    //
    // Author:    Gavin Mogan (halkeye@halkeye.net)
    bool SendToPlayer(std::string message);

    // Function:  GetConnection
    // Returns :  Pointer to the conenction class.
    inline Connection& GetConnection(void) { return *connection; }

    // Function: operator<<
    // Sends Data top player (does color)
    Player& operator<<(const char * message);
    Player& operator<<(const unsigned char * message);
    Player& operator<<(const std::string& message);
    Player& operator<<(char * message);
    // Function: Flush
    // Send data to player (operators are buffered)
    void flush(void);

    // Function:  GetName
    // returns:   returns password
    // Author:    Gavin Mogan (halkeye@halkeye.net)
    std::string GetPassword(void) { return password; };

    // Function:  LookData
    // Arguments: none
    // returns:   string with formatted output of what the Character looks like
    // Author:    Gavin Mogan (halkeye@halkeye.net)
    std::string LookData(void);

    // Function:  GetState
    // Returns:   the current connection state
    // Author:    Gavin Mogan (halkeye@halkeye.net)
    conn_state  GetState(void) { return state; }

    // Function:    SetState
    // Returns: sets the current state
    // Author:  Gavin Mogan (halkeye@halkeye.net)
    void        SetState(conn_state newstate) { state = newstate; }

    // Function:  NextState
    // Returns:   moves ahead to the next state
    // Author:    Gavin Mogan (halkeye@halkeye.net)
    void        NextState(void);

    // Function:     Prompt
    // Description: Send Prompt To Player According to what state they are in
    // Returns:      string of prompt (i think)
    // Author:       Gavin Mogan (halkeye@halkeye.net)
    std::string Prompt(void);

    // Function:    SetPassword
    // Description: Takes a string passed in, and sets the player's password
    // Parameter:   string
    void SetPassword(std::string newpass) { password = newpass; }


    // Function:    GetRoom
    // Description: Returns a pointer to the room the player is currently in
    RoomClass * GetRoom(void) { return inroom; }

    // Function:    ToRoom
    // Description: Sets the room the player is currently in
    void ToRoom(RoomClass * room);

    // Function:    Save
    // Description: Saves player to a file
    bool Save(void);

    // Function:    Load
    // Description: Loads player from a file.
    bool Load(void);

    // Put Object in player inventory
    void AddToPlayer(ObjectClass * o);

    // Take Object from players inventory
    void RemFromPlayer(ObjectClass * o);

    // Find object in inventory
    ObjectClass * FindObject(std::string objname);

    // What level is the user?
    int GetLevel(void);

    // Function:  GetArguments
    // Arguments: none
    // returns:   string - whatever is left in the input buffer
    //
    // Author:    Gavin Mogan (halkeye@halkeye.net)
    std::string GetArguments(void) { return arguments; }

    void SetArguments(std::string newargs) { arguments = newargs; }

    int GetGrade(int classnum);
    virtual bool Update(void);
    
    const object_vector& GetInventory(void) { return inventory; }
    const object_vector& GetEquiped(void) { return equiped; }

    void AddInv(ObjectClass * obj);
    bool exists(const char * username);
};

#endif
