/* vim: set expandtab ts=4 sw=4:
 *
 * Heroes Paradise MUD - area.cpp
 * Copyright (c) 2003 Gavin Mogan <halkeye@halkeye.net>
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (version 2) as published by the
 * Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * This notice must remain unaltered.
 */
#ifndef _AREA_H_
#define _AREA_H_

#include <list>
#include "defines.h"

class RoomClass;

class Area {
public:
    Area();
    bool Save(void);
    bool load(const char * filename);
    RoomClass * FindRoom(int rnum);

public:
    // Filename
    char filename[250];
    // Name
    char name[250];
    // List of rooms
    room_map rooms;
};

#endif
