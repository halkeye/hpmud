/* vim: set expandtab ts=4 sw=4:
 *
 * HARRY POTTER MUD - commands.cpp
 * Copyright (c) 2003 Gavin Mogan <halkeye@halkeye.net>
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (version 2) as published by the
 * Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * This notice must remain unaltered.
 *
 * $Id$
 */

#include "commands.h"
#include "defines.h"
#include "helper.h"
#include "hpmud.h"
#include "player.h"
#include "roomclass.h"
#include "objectclass.h"
#include "logger.h"
#include "helpdata.h"

#include <cstdio>
#include <string>
#include <iomanip>
#include <sstream>
#include <crypt.h>
#include <stdio.h>
#include <string.h>

using std::setw;

void interpret(Player * p) 
{
    p->GetConnection() << NEWLINE;
    switch(p->GetState())
    {
    case STATE_FIRSTNAME:
        {
            p->SetFirstName(p->GetArguments());
            p->NextState();
            p->GetConnection() << hpmud::echo_off_str;
            p->Prompt();
            return;
        }
    case STATE_PASSWORD:
        {
            p->GetConnection() << hpmud::echo_off_str;
            if (p->Load() )
            {
                Player * temp;
                bool link_dead = false;
                RoomClass * temproom = NULL;
                char * result = crypt( p->GetArguments().c_str(), p->GetFirstName().c_str());

                if (strcmp(result,p->GetPassword().c_str()) != 0)
                {
                    p->GetConnection() << "incorrect password";
                    p->Prompt();
                    p->GetConnection() << hpmud::echo_off_str;
                    return;
                }
                // check if user is logging in again.
                if ((temp = game.FindPlayer(p->GetFirstName())) != NULL)
                {
                    // user is already logged in
                    temp->GetConnection() << "Connected from another host" << NEWLINE;
                    temproom = temp->GetRoom();
                    link_dead = true;
                    game.CloseSocket(temp);
                } 
                p->GetConnection() << hpmud::echo_on_str;
                
                // SetState is better
                p->SetState(STATE_PLAYING);
                p->Prompt();
                // Player is loaded, and should be added to the active list
                game.players.insert(make_pair(p->GetFirstName(), p));

                if (!link_dead && !temproom)
                {
                    p->GetRoom()->AddToRoom(p);
                    p->GetRoom()->SendToChar("@W@" + p->GetName() + " appears suddendly" NEWLINE, p);
                } else {
                    // put them back where they were?
                    p->ToRoom(temproom);
                }
                break;
            }
            // encrypting password
            p->SetPassword(crypt(p->GetArguments().c_str(),p->GetFirstName().c_str()));
            p->NextState();
            p->Prompt();
            break;
        }
    case STATE_LASTNAME:
        {
            p->SetLastName(p->GetArguments());
            p->NextState();
            p->Prompt();
            return;
        }
    case STATE_GENDER:
        // Lowercase right? either or
        if (LOWER(p->GetArguments()[0]) == 'm')
            p->SetGender(SEX_MALE);
        else if (LOWER(p->GetArguments()[0]) == 'f')
            p->SetGender(SEX_FEMALE);
        else
        {
            // Just so people know they typed in wrong
            p->GetConnection() << "Invalid Gender, m or f only" << NEWLINE;
            return;
        }
        p->NextState();
        // Player is now finished being created, and should be added to the active list
        game.players.insert(std::make_pair(p->GetFirstName(), p));
        // and moved into a room
        p->GetRoom()->AddToRoom(p);
        // why dpesn't that worrrk?
        p->GetRoom()->SendToChar("@W@" + p->GetName() + " appears suddendly" NEWLINE,p);
        p->Prompt();
        return;
    case STATE_PLAYING:
        {
            command_hash_type::const_iterator command_iter;
            /* FIX ME */
            std::string cmd;
            std::string args = p->GetArguments();
            cmd = GetWord(args);
            p->SetArguments(args);
            command_iter = game.command_hash.find(ToLower(cmd));
            if (command_iter != game.command_hash.end() &&
                (command_iter->second->level == USER_ALL ||
                command_iter->second->level & p->GetLevel()) )
            {
                if (command_iter->second->function(*p, p->GetArguments()) != -1)
                    p->Prompt();

                return;
            }

            for( command_iter = game.command_hash.begin(); command_iter != game.command_hash.end(); ++command_iter)
            {
                commandinfo * cmdinfo = command_iter->second;
                if ( cmdinfo && str_prefix(cmd,cmdinfo->name) &&
                    (cmdinfo->level == USER_ALL ||
                    cmdinfo->level & p->GetLevel()) )
                {
                    if (cmdinfo->function(*p,p->GetArguments()) != -1)
                        p->Prompt();
                    return;
                }
            }

            p->GetConnection() << "huh?" << NEWLINE;
            p->Prompt();
            break;
        }
    default:
        Logger lg;
        lg << "HUH?" << NEWLINE;
    }
    
    return;
}


/* part of player class? */
int cmd_help(Player& p, std::string args)
{
    /*helpdata * hd;
    if (!args.empty())
    {
        hd = game.connection->getHelp(args.c_str(), true);
        if (hd != NULL)
        {
            p << NEWLINE;
            p << "@G@" << hd->keyword << "@c@" << NEWLINE;
            p << hd->data << NEWLINE;
            if (hd->bdate) {
                p << "Last Updated: " << hd->lastupdated << NEWLINE;
            }
            delete hd;
            return 1;
        }
        p << "No Such Help Entry." << NEWLINE;
        return 1;
    }*/
    p << "Help with what?" << NEWLINE;
    return 1;
}

int cmd_quit(Player& p, std::string args)
{
    p << "Hope to see you soon." << NEWLINE;
    p.GetRoom()->SendToChar("@W@" + p.GetName() + " leaves in a puff of smoke into nothingness. " NEWLINE,&p);
    p.Save();
    game.CloseSocket(&p);
    return -1;
}


int cmd_say(Player& p, std::string args)
{
    std::string message;

    p << "@C@You say \"";
    // what person says here
    p << args;
    p << "@C@\"" << NEWLINE;

    message = "@C@";
    message += p.GetProperName() + " says \"@C@";
    // what person says here
    message += args + "@C@\"";
    message += NEWLINE;
    p.GetRoom()->SendToChar(message, &p);
    return 1;
}

int cmd_north(Player& p, std::string args)
{
    RoomClass * room = p.GetRoom()->FindExit(EXIT_NORTH);
    if (room != NULL)
    {
        p.GetRoom()->SendToChar(p.GetProperName() + " moves north." NEWLINE, &p);
        p.ToRoom(room);
        p.GetRoom()->SendToChar(p.GetProperName() + " comes in from the south." NEWLINE, &p);
        p << "You move north." << NEWLINE;
    return 1;
    }
    p << "You can't go that way." << NEWLINE;
    return 1;
}

int cmd_south(Player& p, std::string args)
{
    RoomClass * room = p.GetRoom()->FindExit(EXIT_SOUTH);
    if (room != NULL)
    {
        p.GetRoom()->SendToChar(p.GetProperName() + " moves south." NEWLINE, &p);
        p.ToRoom(room);
        p.GetRoom()->SendToChar(p.GetProperName() + " comes in from the north." NEWLINE, &p);
        return p.SendToPlayer("You move south." NEWLINE);
    }
    return p.SendToPlayer("You can't go that way." NEWLINE);
}

// Move East
int cmd_east(Player& p, std::string args)
{
    RoomClass * room = p.GetRoom()->FindExit(EXIT_EAST);
    if (room != NULL)
    {
        p.GetRoom()->SendToChar(p.GetProperName() + " moves east." NEWLINE, &p);
        p.ToRoom(room);
        p.GetRoom()->SendToChar(p.GetProperName() + " comes in from the west." NEWLINE, &p);
        return p.SendToPlayer("You move east." NEWLINE);
    }
    return p.SendToPlayer("You can't go that way." NEWLINE);
}
// Move West
int cmd_west(Player& p, std::string args)
{
    RoomClass * room = p.GetRoom()->FindExit(EXIT_WEST);
    if (room != NULL)
    {
        p.GetRoom()->SendToChar(p.GetProperName() + " moves west." NEWLINE, &p);
        p.ToRoom(room);
        p.GetRoom()->SendToChar(p.GetProperName() + " comes in from the east." NEWLINE, &p);
        return p.SendToPlayer("You move west." NEWLINE);
    }
    return p.SendToPlayer("You can't go that way." NEWLINE);
}

int cmd_save(Player& p, std::string args)
{
    if( p.Save() )
    {
        return p.SendToPlayer("Saved." NEWLINE);
    }
    return p.SendToPlayer("ERROR! REPORT TO Gavin" NEWLINE);
}

int cmd_commands(Player& p, std::string args)
{
    std::string ret;
    command_hash_type::iterator command_iter;   
    commandinfo * cmd;
    int cmd_count = 0;
    std::stringstream ss;
    ss.setf(std::ios::left);
    
    ret = "HP MUD COMMANDS" NEWLINE;
    ret += "===============" NEWLINE;
    
    /* Process commands */
    for (command_iter = game.command_hash.begin(); command_iter != game.command_hash.end() ;++command_iter)
    {
        cmd = command_iter->second;
        if ((cmd->level == USER_ALL || 
            cmd->level & p.GetLevel() ) &&
            cmd->name.compare(command_iter->first) == 0 )
        {
            if (cmd->level != USER_ALL)
                ss << "@R@";
            ss << setw(20) << command_iter->first.c_str();
            if (cmd->level != USER_ALL)
                ss << "@c@";
            if ((++cmd_count % 4) == 0)
                ss << NEWLINE;
        }
    }
    ret += ss.str();
    ret += NEWLINE;         
    return p.SendToPlayer(ret);
}

int cmd_inventory(Player& p, std::string args)
{
    std::string ret;
        
    ret = "@W@Inventory" NEWLINE;
    ret += "=========@G@" NEWLINE;
    if (!p.GetInventory().empty()) 
    {
        object_vector::const_iterator i_obj = p.GetInventory().begin();
        ObjectClass * obj;
        for( ; i_obj != p.GetInventory().end(); ++i_obj)
        {
            obj = (*i_obj);
            if (obj)
            {
                ret += obj->GetName() + NEWLINE;
            }
        }
    }

    return p.SendToPlayer(ret);
}

int cmd_emote(Player& p, std::string args)
{
    std::string ret;
    if (args.empty())
    {
        return p.SendToPlayer("emote what?");
    }

    ret = "You " + args + NEWLINE;
    p.SendToPlayer(ret );
    ret = p.GetProperName() + " " + args  + NEWLINE;
    return p.GetRoom()->SendToChar(ret,&p);
}
#define _VERSION "VERSION"
int cmd_version(Player& p, std::string args)
{
    std::string ret;
    char temp[10];
    std::stringstream ss;
    ss << "@Y@HPMUD Version :" << _VERSION;
    ss << NEWLINE;

    //sprintf(temp, "%f", VERSION);
    ret += temp;
    ret += "undefined";
    ret += NEWLINE;

    ret = ss.str();
    return p.SendToPlayer(ret);
}

int cmd_shutdown(Player& p, std::string args)
{
    game.Shutdown();
    return p.SendToPlayer("Preparing to shutdown" NEWLINE);
}
