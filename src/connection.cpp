/* vim: set expandtab ts=4 sw=4:
 *
 * HARRY POTTER MUD - connection.cpp
 * Copyright (c) 2003 Gavin Mogan <halkeye@halkeye.net>
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (version 2) as published by the
 * Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * This notice must remain unaltered.
 *
 * $Id$
 */

#include "connection.h"
#include "player.h"

#include <netdb.h>
#include <arpa/inet.h>
#include <sys/types.h>

#include <string>

Connection::Connection(unsigned int socketd)
{
    struct sockaddr_in addr;
    struct hostent *h;
    unsigned int length;

    socket = socketd;

    length = sizeof(struct sockaddr);
    getpeername(socketd,(struct sockaddr *)&addr, &length);
    ip = inet_ntoa(addr.sin_addr);
    h = gethostbyaddr((struct sockaddr *)&addr, length, AF_INET);
}

std::string Connection::GetIp(void)
{
    return ip;
}

/* Connection Sending, no ansi */
Connection& Connection::operator<<(const char * message)
{
    SendToPlayer(message);
    return *this;
}

Connection& Connection::operator<<(const unsigned char * message)
{
    SendToPlayer(reinterpret_cast<const char *>( message ));
    return *this;
}

Connection& Connection::operator<<(const std::string& message)
{
    SendToPlayer(message);
    return *this;
}

void Connection::SendToPlayer(std::string message)
{
    int total=0;
    int bytesleft=0;
    int length;
    int n = -1;

    /* Connection << operator doesn't do color */
    length = message.length();
    
    bytesleft = length;

    while (total < length) {
        n = send(socket, (message.c_str())+total, bytesleft, 0);
        if (n == -1) { perror("send"); break; }
        total += n;
        bytesleft -= n;
    }
    return;
}

