/* vim: set expandtab ts=4 sw=4:
 *
 * HARRY POTTER MUD - chracter.h
 * Copyright (c) 2003 Gavin Mogan <halkeye@halkeye.net>
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (version 2) as published by the
 * Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * This notice must remain unaltered.
 *
 * $Id$
 */

#ifndef _CHARACTER_H_
#define _CHARACTER_H_

#include <string>
#include <vector>
#include "defines.h"

/**
 * @short Character Class.
 * @author Gavin Mogan <halkeye@halkeye.net>
 * @version $Id$
 */
class Character
{
public:
    /**
     * Default Constructor
     */
    Character(void);

    /**
     * Default Destructor
     */
    virtual ~Character(void);
    
    /**
     * Name in the format of "Firstname Lastname"
     * @return string "Firstname Lastname"
     */
    std::string GetName(void);

    /**
     * GetProperName
     * Returns:     Name in the format of "GenderNoun. Lastname"
     * Author:      Gavin Mogan (halkeye@halkeye.net)
     */
    std::string GetProperName(void);

    /**
     * Function:    GetFirstName
     * Returns:     string of first name
     * Author:      Gavin Mogan (halkeye@halkeye.net)
     */
    std::string GetFirstName(void) { return firstname; }

    /**
     * Function:    GetLastName
     * Returns:     string of lastname
     * Author:      Gavin Mogan (halkeye@halkeye.net)
     */
    std::string GetLastName(void) { return lastname; }
    
    /**
     * Function:    SetFirstName
     * Description: Takes a string passed in, and sets the player's first name
     * Parameter:   string
     * */
    void SetFirstName(std::string name) {
        /* No error checking is needed, so inline is good */
        firstname = name;
    }
    
    /**
     * Function:    SetLastnameName
     * Description: Takes a string passed in, and sets the player's last name
     * Parameter:   string
     */
    void SetLastName(std::string name) {
        /* No error checking is needed, so inline is good */
        lastname = name;
    }


    /**
     * Function:    IsName
     * Description: Preforms a case insensitve search for the players name
     * Return:      true if it is, false otherwise
     */
    bool IsName(std::string name);
    
    /**
     * Sets the Gender of a Character
     * Author: Gavin Mogan <halkeye@halkeye.net>
     */
    void SetGender(gender_type newgender) { gender = newgender; }

    /**
     * Retrieves the Gender of a Character
     */
    gender_type GetGender(void) { return gender; }
    
private:
    // gender
    gender_type gender;
    // player firstname
    std::string firstname;
    // player lastname
    std::string lastname; /* "Mr. Potter" - snape */
    // Equipment
    object_vector equiped;
};

#endif
