/* vim: set expandtab ts=4 sw=4:
 *
 * HARRY POTTER MUD - logger.cpp
 * Copyright (c) 2003 Gavin Mogan <halkeye@halkeye.net>
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (version 2) as published by the
 * Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * This notice must remain unaltered.
 *
 * $Id$
 */

#include "logger.h"
#include <iostream>

Logger::Logger()
{
    type = LOG_STANDARD;
}

std::ostream& Logger::operator<<(const std::string& msg)
{
    switch (type)
    {
        case LOG_STANDARD:
            std::cerr << "[STANDARD] " << msg;
            break;
        case LOG_ERROR:
            std::cerr << "[ERROR] " << msg;
            break;
    }
    
    type = LOG_STANDARD;
    return std::cerr;
}

Logger& Logger::operator<<(e_logger l)
{
    SetType(l);
    return *this;
}

Logger& Logger::SetType(e_logger t)
{
    type = t;
    return *this;
}
