/* vim: set expandtab ts=4 sw=4:
 *
 * HARRY POTTER MUD - helper.h
 * Copyright (c) 2003 Gavin Mogan <halkeye@halkeye.net>
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (version 2) as published by the
 * Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * This notice must remain unaltered.
 *
 * $Id$
 */

#ifndef _HELPER_H_
#define _HELPER_H_

#include <string>

/*
 * Function: GetWord
 *
 * Returns: word if there is a space in the string, "' else
 *
 * Side Effects: remaining is stored back into fullstring
 */
std::string GetWord(std::string &fullstring);

std::string GetWord2(std::string fullstring);

/*
 * Function: ToLower
 *
 * Returns: returns the passed in string but in lowercase
 */
std::string ToLower(const std::string str);

std::string UCFirst(const std::string str);

bool str_prefix( const std::string astr, const std::string bstr );

#endif
