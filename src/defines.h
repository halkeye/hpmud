/* vim: set expandtab ts=4 sw=4:
 *
 * HARRY POTTER MUD - defines.h
 * Copyright (c) 2003 Gavin Mogan <halkeye@halkeye.net>
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (version 2) as published by the
 * Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * This notice must remain unaltered.
 *
 * $Id$
 */

#ifndef _DEFINES_H_
#define _DEFINES_H_

#include <string>

#define MAX_SIZE        1024

#ifndef MAX
#define MAX(a,b) ((a)>(b)?(a):(b))
#endif
#ifndef NEWLINE
#define NEWLINE "\n\r"
#endif

/*
 * Utility macros.
 */
#define UMIN(a, b)              ((a) < (b) ? (a) : (b))
#define UMAX(a, b)              ((a) > (b) ? (a) : (b))
#define URANGE(a, b, c)         ((b) < (a) ? (a) : ((b) > (c) ? (c) : (b)))
#define LOWER(c)                ((c) >= 'A' && (c) <= 'Z' ? (c)+'a'-'A' : (c))
#define UPPER(c)                ((c) >= 'a' && (c) <= 'z' ? (c)+'A'-'a' : (c))

/*
 * list typedefs
 */
#include <list>
#include <vector>
#include <map>

class Player;
class ExitClass;
class RoomClass;
class ObjectClass;
class Connection;
class Area;

typedef std::list<Player *>     player_list;
typedef std::list<ExitClass *>  exit_list;
typedef std::list<RoomClass *>  room_list;
typedef std::list<ObjectClass *>    object_list;
typedef std::list<Area *>    area_list;

typedef std::vector<ObjectClass *>    object_vector;

typedef std::map<std::string, Player *>     player_map;
typedef std::map<std::string, ExitClass *>  exit_map;
typedef std::map<int, RoomClass *>      room_map;
typedef std::map<std::string, ObjectClass *>    object_map;
typedef std::map<int, Connection>       connection_map;

enum gender_type {SEX_MALE, SEX_FEMALE};
enum conn_state { STATE_FIRSTNAME, STATE_PASSWORD, STATE_LASTNAME, STATE_GENDER, STATE_PLAYING };
enum exit_types { EXIT_NORTH, EXIT_SOUTH, EXIT_WEST, EXIT_EAST };
enum house_types { HOUSE_SLYTHERIN, HOUSE_GRYFFINDOR, HOUSE_RAVENCLAW, HOUSE_HUFFLEPUFF };
enum user_types { USER_ALL = 0, USER_STUDENT = 1 << 0, USER_TEACHER = 1 << 1, USER_GOD = 1 << 2 };
enum e_wearloc { WEAR_NONE = 0, WEAR_HEAD, MAX_WEARLOC };
enum e_classes { CLASS_POTIONS, MAX_CLASSES };
/* Char * Const */
extern char * const classes[];
extern char * const wearlocs[];

#endif
