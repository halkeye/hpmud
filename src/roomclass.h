/* vim: set expandtab ts=4 sw=4:
 *
 * HARRY POTTER MUD - roomclass.h
 * Copyright (c) 2003 Gavin Mogan <halkeye@halkeye.net>
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (version 2) as published by the
 * Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * This notice must remain unaltered.
 *
 * $Id$
 */

#ifndef _ROOMCLASS_H_
#define _ROOMCLASS_H_

#include <list>
#include <string>
#include "defines.h"
#include "exitclass.h"
#include "updatable.h"
#include <fstream>

// class prototyping
class Player;
class ExitClass;
class ObjectClass;
class Area;

/*
 * inherit lookabaleObject
 *
 * virtual std::string LookData(void) = 0;
 */
class RoomClass:
    public Updatable
{
    
protected:
    // room name
    std::string name;
    // room descripton
    std::string description;
    // room identification number
    int rnum;
    // Which area are we in?
    Area * inarea;

public:
    // Create default room
    RoomClass(int num);
    // Delete room
    virtual ~RoomClass(void);
    // exits
    exit_list exits;
    // players in room
    player_map players;
    // Objects in room
    object_map objects;
    
public: // functions
    void SetName(std::string newname) { name = newname; }
    void SetDesc(std::string newdesc) { description = newdesc; }
    void SetRnum(int num) { rnum = num; }

    std::string GetName(void) { return name; }
    std::string GetDesc(void) { return description; }
    int GetRnum(void) { return rnum; }

    // Returns generic data about the room
    std::string LookData(void);
    // returns character specific information about a room (ie if player 
    // owns the room, one thing, otherwise generic? doesn't show themselves in the room)
    std::string LookData(Player * p);

    // Put Character in room
    void AddToRoom(Player * p);
    // Take Char from room
    void RemFromRoom(Player * p);

    // Put Object in room
    void AddToRoom(ObjectClass * o);
    // Take Object from room
    void RemFromRoom(ObjectClass * o);


    // Create a new room
    RoomClass * CreateRoom(int num);
    // Create an exit between a room
    ExitClass * CreateExit(RoomClass * room, exit_types dir, int exitnum = 0);
    // Sends Message to all players in the room (except nosend)
    int SendToChar(std::string message, Player * nosend = NULL);
    // finds and returns the exit in said direction
    RoomClass * FindExit(exit_types dir);
    // Find a player in a room
    Player * FindPlayer(std::string name);
    // find object in a room
    ObjectClass * FindObject(std::string name);
    // Save
    bool Save(std::ofstream & out);
    // Which area are we in?
    Area * GetArea(void) { return inarea; }
    
    // For Updatable Object
    virtual bool Update(void);
};

#endif
