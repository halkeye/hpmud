/* vim: set expandtab ts=4 sw=4:
 *
 * HARRY POTTER MUD - helper.cpp
 * Copyright (c) 2003 Gavin Mogan <halkeye@halkeye.net>
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (version 2) as published by the
 * Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * This notice must remain unaltered.
 *
 * $Id$
 */

#include "helper.h"
#include "defines.h"
#include <string>
#include <cctype>
#include <algorithm> // for transform()

std::string GetWord(std::string &fullstring)
{
    unsigned int loc = fullstring.find( " ", 0 );
    
    if( loc != std::string::npos )
    {
        /* Rest should be the first letter after space */
        std::string rest(fullstring, loc+1, fullstring.length()-loc-1);
        std::string ret(fullstring, 0, loc);
        fullstring = rest;
        return ret;
    }
    else
    {
        // there is no space....
        // return blank
        std::string ret = fullstring;
        fullstring = "";
        return ret;
    }
}

// keep fullstring
std::string GetWord2(std::string fullstring)
{
    unsigned int loc = fullstring.find( " ", 0 );
    
    if( loc != std::string::npos )
    {
        /* Rest should be the first letter after space */
        std::string ret(fullstring, 0, loc);
        return ret;
    }
    else
    {
        // there is no space....
        return "";
    }
}

std::string ToLower(const std::string str)
{
    std::string ret = str;
    transform(ret.begin(), ret.end(), ret.begin(), tolower);
    return ret;
}

std::string UCFirst(const std::string str)
{
    std::string ret = str;
    transform(ret.begin(), ret.end(), ret.begin(), tolower);
    ret[0] = UPPER(ret[0]);
    return ret;
}

/*
 * Compare strings, case insensitive, for prefix matching.
 */
bool str_prefix( std::string astr, std::string bstr )
{
    /* Lets assume astr and bstr came in properly */
    unsigned int x, length = astr.length();

    if (length > bstr.length())
        return false;

    astr = ToLower(astr);
    bstr = ToLower(bstr);
    for ( x = 0; x < length; ++x )
    {
        if ( astr.at(x) != bstr.at(x) )
        {
            return false;
        }
    }

    return true;
}
