/* vim: set expandtab ts=4 sw=4:
 *
 * HARRY POTTER MUD - olc_commands.cpp
 * Copyright (c) 2003 Gavin Mogan <halkeye@halkeye.net>
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (version 2) as published by the
 * Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * This notice must remain unaltered.
 *
 * $Id$
 */

#include "commands.h"
#include "player.h"
#include "hpmud.h"
#include "roomclass.h"
#include <string.h>
#include <stdlib.h>

int cmd_redit(Player& p, std::string args)
{
#if 0
    std::string cmd = GetWord(args);
    if (!ToLower(cmd).compare("save"))
    {
        if (p.GetRoom()->Save())
            p << "Saved." << NEWLINE;
        else
            p << "Error saving." << NEWLINE;
        return 1;
    }
    if (!ToLower(cmd).compare("name") && !args.empty())
    {
        p.GetRoom()->SetName(args);
        p << "Set." << NEWLINE;
        return 1;
    }
    if (!ToLower(cmd).compare("desc") && !args.empty())
    {
        p.GetRoom()->SetDesc(args);
        p << "Set." << NEWLINE;
        return 1;
    }
    if (!ToLower(cmd).compare("exit"))
    {
        RoomClass * room;
        cmd = GetWord(args);
        
        if (!ToLower(cmd).compare("add"))
        {
            std::string direction = GetWord(args);
            std::string roomnum = GetWord(args);

            if (!roomnum.empty() && !direction.empty())
            {
                room = game.FindRoom(atoi(roomnum.c_str()));
                if (!room)
                {
                    p << "Cannot find room" << NEWLINE;
                    return 0;
                }
                if (!direction.compare("north") || cmd[0] == 'n')
                    p.GetRoom()->CreateExit(room,EXIT_NORTH)->Save();
                else if (!direction.compare("south") || cmd[0] == 's')
                    p.GetRoom()->CreateExit(room,EXIT_SOUTH)->Save();
                else if (!direction.compare("east") || cmd[0] == 'e')
                    p.GetRoom()->CreateExit(room,EXIT_EAST)->Save();
                else if (!direction.compare("west") || cmd[0] == 'w')
                    p.GetRoom()->CreateExit(room,EXIT_WEST)->Save();
                else
                {
                    p << "which direction?" << NEWLINE;
                    return 0;
                }
                p.GetRoom()->Save();
                return 1;
            }
        }
        else if (!ToLower(cmd).compare("remove"))
        {
            p << "Unimplemented" << NEWLINE;
            return 1;
        }
        else if (!ToLower(cmd).compare("save"))
        {
            p << "Unimplemented" << NEWLINE;
            return 1;
        }
        /* should get areas working asap */

        p << "redit exit <add/remove/save> <direction> [room#]" << NEWLINE;
        return 1;
    }
#endif
    p << "redit <command> [arguments]" << NEWLINE;
    p << "Current Options:" << NEWLINE;
    p << "\tname\tdesc\tsave\texit" << NEWLINE;
        
    return 1;
}

/*
 * mememememe
 *
 * Goto works in the area we are in
 * or area assigned to us..
 *
 * After that, you must switch areas? then goto that?
 * no.. thats messy.. bah
 */
int cmd_goto(Player& p, std::string args)
{
    RoomClass * room;
    std::string roomnum = args;
    

    roomnum = GetWord(roomnum);
    room = game.FindRoom(atoi(roomnum.c_str()));
    p.GetRoom()->SendToChar(p.GetProperName() + " Disappears in a puff of smoke" NEWLINE, &p);

    if (room == NULL)
    {
        room = p.GetRoom()->CreateRoom(atoi(roomnum.c_str()));
        p.ToRoom(room);
    }
    else
    {
        p.ToRoom(room);
        // don't need todo it if you create the room.. 'cause there's no way someone else is there
        p.GetRoom()->SendToChar(p.GetProperName() + " Appears in a puff of smoke" NEWLINE, &p);
    }
    return 1;
}
