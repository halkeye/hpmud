/* vim: set expandtab ts=4 sw=4:
 * 
 * HARRY POTTER MUD - player.cpp
 * Copyright (c) 2003 Gavin Mogan <halkeye@halkeye.net>
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (version 2) as published by the
 * Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * This notice must remain unaltered.
 *
 * $Id$
 */

#include "player.h"
#include "connection.h"
#include "defines.h"
#include "helper.h"
#include "hpmud.h"
#include "roomclass.h"
#include "objectclass.h"
#include "logger.h"

#include "simplexml.h"

#include <cstdio>
#include <fstream>
#include <sys/types.h>
#include <sys/socket.h>
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <stdlib.h>

Player::Player(unsigned int socketd)
{
    /* Create connection data and set the socket # */
    connection = new Connection(socketd);
    connection->SetPlayer(this);
    /* Default State */
    state = STATE_FIRSTNAME;
    /* Default Firstname */
    SetFirstName("Unconnected");
    /* Default Last Name */
    SetLastName("Player");
    /* Default Gender */
    SetGender(SEX_MALE);
    /* Default Password */
    password = "";
    /* Default Room */
    inroom = NULL;
    inroom = hpmud::testroom;
    //game.FindRoom(1);
    /* Default Level */
    level = 1;
    /* Loaded Yet? */
    loaded = false;
}

Player::~Player(void)
{
    if (connection)
        delete connection;
    if (inroom)
    {
        inroom->RemFromRoom(this);
        inroom = NULL;
    }
}

int Player::GetLevel(void)
{
    return level;
}

Player& Player::operator<<(const char * message)
{
    outbuf.append(message);
    return *this;
}

Player& Player::operator<<(const unsigned char * message)
{
    outbuf.append( reinterpret_cast<const char *>( message ), sizeof( message ) );
    return *this;
}

Player& Player::operator<<(const std::string& message)
{
    outbuf.append(message);
    return *this;
}

Player& Player::operator<<(char * message)
{
    outbuf.append(message);
    return *this;
}


void Player::flush(void)
{
    SendToPlayer(outbuf);
    outbuf = "";
    return;
}

bool Player::SendToPlayer(std::string message)
{
    int socketd = GetConnection().GetSocket();
    
    int total=0;
    int bytesleft=0;
    int length;
    int n = -1;

    std::size_t loc;

    // Red
    while ((loc = message.find("@R@")) != std::string::npos)
    {
        message = message.erase(loc+1, 2);
        message.replace(loc,1,"\033[1m\033[31m");
    }

    // green
    while ((loc = message.find("@G@")) != std::string::npos)
    {
        message = message.erase(loc+1, 2);
        message.replace(loc,1,"\033[1m\033[32m");
    }

    // yellow 
    while ((loc = message.find("@Y@")) != std::string::npos)
    {
        message = message.erase(loc+1, 2);
        message.replace(loc,1,"\033[1m\033[33m");
    }

    // blue
    while ((loc = message.find("@B@")) != std::string::npos)
    {
        message = message.erase(loc+1, 2);
        message.replace(loc,1,"\033[1m\033[34m");
    }
    
    // purple
    while ((loc = message.find("@P@")) != std::string::npos)
    {
        message = message.erase(loc+1, 2);
        message.replace(loc,1,"\033[1m\033[35m");
    }

    // cyan
    while ((loc = message.find("@C@")) != std::string::npos)
    {
        message = message.erase(loc+1, 2);
        message.replace(loc,1,"\033[1m\033[36m");
    }

    // white
    while ((loc = message.find("@W@")) != std::string::npos)
    {
        message = message.erase(loc+1, 2);
        message.replace(loc,1,"\033[1m\033[37m");
    }

    // &c = reset color
    while ((loc = message.find("@c@")) != std::string::npos)
    {
        message = message.erase(loc+1, 2);
        message.replace(loc,1,"\033[0m"); // Resets color
    }

    if (message[message.length()-1] == '\n' ||
        message[message.length()-1] == '\r')
        message += "\033[0m"; // reset to default   
    length = message.length();
    
    bytesleft = length;

    while (total < length) {
        n = send(socketd, (message.c_str())+total, bytesleft, 0);
        if (n == -1) { perror("send"); break; }
        total += n;
        bytesleft -= n;
    }
    return ((n == -1)?false:true);
}

std::string Player::LookData(void)
{
    std::string ret;
    ret = "Name: " + GetProperName() + NEWLINE;
    return ret;
}

void Player::NextState(void)
{
    switch(state)
    {
    case STATE_FIRSTNAME:
        state = STATE_PASSWORD;
        break;
    case STATE_PASSWORD:
        state = STATE_LASTNAME;
        break;
    case STATE_LASTNAME:
        state = STATE_GENDER;
        break;
    case STATE_GENDER:
        state = STATE_PLAYING;
        break;
    case STATE_PLAYING:
        break;
    }
    return;
}

std::string Player::Prompt(void)
{
    std::string ret;
    switch(GetState()) {
    case STATE_FIRSTNAME:
        ret = NEWLINE "Enter Firstname(username): ";
        break;
    case STATE_PASSWORD:
        ret = NEWLINE "Password: "; /* Hide? */
        //ret += echo_off_str;
        break;
    case STATE_LASTNAME:
        ret = NEWLINE "Enter Lastname: ";
        break;
    case STATE_GENDER:
        ret = NEWLINE "Enter Gender[m|f]: ";
        break;
    case STATE_PLAYING:
        ret = NEWLINE "Command: ";
        break;
    }

    SendToPlayer(ret);
    flush();
    return ret;
}

// Function:    ToRoom
// Description: Sets the room the player is currently in
void Player::ToRoom(RoomClass * room) { 
    // remove from old room
    if (inroom != NULL)
        inroom->RemFromRoom(this);
    // change location
    inroom = room; 
    // add to new room
    room->AddToRoom(this); 
}

// Function:    Save
// Description: Saves player to a file
bool Player::Save(void)
{
    // should change eventually no?
    std::string username = GetFirstName(); 
    if (state == STATE_PLAYING) // dont' save if not done setting up
    {
        char filename[250];
        FILE * fp = NULL;
        sprintf(filename, "../players/%s", username.c_str());
        std::ofstream out(filename);
        out << "<user version=1>\n";
        out << "  <firstname>" << GetFirstName() << "</firstname>\n";
        out << "  <lastname>" << GetLastName() << "</lastname>\n";
        out << "  <password>" << GetPassword() << "</password>\n";
        out << "  <gender>" << (int)GetGender() << "</gender>\n";
        out << "  <level>" << level << "</level>\n";
        out << "</user>\n";
        // Save Crap
        out.close();
    }
    return true;
}

// Function:    Load
// Description: Loads player from a file.
bool Player::Load(void)
{
    char filename[250];
    FILE * fp = NULL;
    std::string str;
    std::string temp;
   
    if (loaded) return true;

    const char * username = GetFirstName().c_str(); // Replace with username
    sprintf(filename, "../players/%c%s", toupper(username[0]), username+1);

    std::ifstream in(filename);
    if ( !in ) {
        Logger err;
        err << "Error: Can't open file " << filename << "\n";
        return false;
    }
   
    str = "";
    getline(in,temp);  // Get the frist line from the file, if any.
    while ( in ) {  // Continue if the line was sucessfully read.
        str += temp;  // Process the line.
        getline(in,temp);   // Try to get another line.
    }
    
    simplexml *root = new simplexml(str.c_str());
    simplexml *user = root; //->child("User");
    if (user) {
        SetFirstName(user->child("firstname")->value());
        SetLastName(user->child("lastname")->value());
        password = user->child("password")->value();
        SetGender((gender_type)atoi(user->child("gender")->value()) );
        level = atoi(user->child("level")->value());
        delete root;
    }
   
    // returns false if no
    loaded = true;
    return true;
}


// Put Object in player inventory
void Player::AddToPlayer(ObjectClass * o)
{
    inventory.insert(inventory.end(),o);
}

// Take Object from players inventory
void Player::RemFromPlayer(ObjectClass * o)
{
    object_vector::iterator i;
    for( i = inventory.begin(); i != inventory.end(); ++i)
    {
        ObjectClass * obj = (*i);
        if (obj && obj == o)
        {
            i = inventory.erase(i);
            break;
        }
    }
}

ObjectClass * Player::FindObject(std::string objname)
{
    // abstract this...
    // pointer to object list (or map)
    object_vector::iterator i;
    for( i = inventory.begin(); i != inventory.end(); ++i)
    {
        ObjectClass * obj = (*i);
        if ( ToLower(objname).compare(ToLower(obj->GetName())) == 0)
        {
            return obj;
        }
    }
    // we didn't find what we wanted in the player's inventory
    // so try word by word search
    std::string word = ToLower(objname);
    for( i = inventory.begin(); i != inventory.end(); ++i)
    {
        ObjectClass * obj = (*i);
        if ( str_prefix(word, obj->GetName() ) )
        {
            return obj;
        }
    }
    return NULL;
}

int Player::GetGrade(int classnum)
{
    return -1;
}

bool Player::Update(void)
{
    return true; // updated fine
}

void Player::AddInv(ObjectClass * obj)
{
    equiped.insert(equiped.begin(),obj);
    return;
}

bool Player::exists(const char * username)
{
    char filename[250];
    FILE * fp = NULL;
    sprintf(filename, "../players/%c%s", toupper(username[0]), username+1);
    Logger l; l << "Filename: (" << filename << ")\n";
    fp = fopen(filename, "r");
    if (fp) { fclose(fp); return true; }
    return false;
}
