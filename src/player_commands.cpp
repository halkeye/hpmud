/* vim: set expandtab ts=4 sw=4:
 *
 * HARRY POTTER MUD - player_commands.cpp
 * Copyright (c) 2003 Gavin Mogan <halkeye@halkeye.net>
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (version 2) as published by the
 * Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * This notice must remain unaltered.
 *
 * $Id$
 */

#include "commands.h"
#include "player.h"
#include "defines.h"
#include "hpmud.h"
#include "objectclass.h"
#include <sstream>
#include <iomanip>

int cmd_score(Player& p, std::string args)
{
    std::stringstream temp;
    unsigned short int count;

    temp << p.GetName() << NEWLINE;
    temp << "=============================================";
    temp << NEWLINE;
    for (count = 0; count < MAX_CLASSES; ++count)
    {
        temp << std::setw(12) << std::setiosflags(std::ios::left) << classes[count] << " - ";
        temp << p.GetGrade(count) << NEWLINE; // Until next? take exams to level up
    }

    return p.SendToPlayer(temp.str());
}

int cmd_look(Player& p, std::string args)
{
    std::string output = args;

    if (args.empty() )
    { 
        RoomClass * room = p.GetRoom();
        if (room)
        {
            output = room->LookData();
            output += room->LookData(&p);
        }
        else
            output = "Not in a room, sorry" NEWLINE;
    } else {
        std::string name = GetWord(args);
        Player * temp;
        if ((temp = p.GetRoom()->FindPlayer(name)) == NULL)
        {
            ObjectClass * obj;
            if ((obj = p.GetRoom()->FindObject(name)) == NULL)
                                output = "Who?" NEWLINE;
            else
                output = obj->LookData();
        }
        else // if look for object
            output = temp->LookData();
    }
    p.SendToPlayer(output);
    return 0;
}

int cmd_who(Player& p, std::string args)
{
    std::string message;
    std::stringstream ss;

    ss << "+-------------------------------------------+" << NEWLINE;
    ss << "|       Players of Harry Potter Mud         |" << NEWLINE;
    ss << "+-------------+-----------------------------+" << NEWLINE;
//    ss << "| Name        | HOUSE                       |" << NEWLINE;
    ss << "+-------------+-----------------------------+" << NEWLINE;
    /* Loop */
    connection_map::iterator i;
    for( i = game.connections.begin(); i != game.connections.end(); ++i)
    {
        Player& p = i->second.GetPlayer();
        if (p.GetState() == STATE_PLAYING)
        {
            ss.fill(' ');
            ss << "| ";
            ss << std::setw(11) << p.GetName().c_str();
            //ss << std::setw(1) << " | " << p.GetHouseColor();
            //ss << std::setw(27) << std::setiosflags(std::ios::left) << p.GetHouse();
            ss << std::setw(1) << " @c@|" << NEWLINE;
        }
    }
    /* Done looping */
    ss << "+-------------+-----------------------------+" << NEWLINE;
    return p.SendToPlayer(ss.str());

}

int cmd_get(Player& p, std::string args)
{
    RoomClass * room = p.GetRoom();

    if (room && !args.empty())
    {
        ObjectClass * obj;
        // is it in the room?
        if ((obj = room->FindObject(args)) != NULL)
        {
            // remove from room
            room->RemFromRoom(obj);
            // Add to player's inventory
            p.AddToPlayer(obj);

            p.SendToPlayer("Picked up " + obj->GetName() + NEWLINE);
            p.GetRoom()->SendToChar(p.GetProperName() + " picked up " + obj->GetName() + NEWLINE,&p);
            return 1;
        }
    } 
    return p.SendToPlayer("Get what?" NEWLINE);
}

int cmd_drop(Player& p, std::string args)
{
    if (!args.empty())
    {
        ObjectClass * obj;
        // is it in the room?
        if ((obj = p.FindObject(args)) != NULL)
        {
            // remove from room
            p.RemFromPlayer(obj);
            // Add to player's inventory
            p.GetRoom()->AddToRoom(obj);

            p << "Dropped " << obj->GetName() << NEWLINE;
            p.GetRoom()->SendToChar(p.GetProperName() + " dropped " + obj->GetName() + NEWLINE, &p);
            return 1;
        }
    } 
    return p.SendToPlayer("Drop what?" NEWLINE);
}

// Equip
// Wear
int cmd_equip(Player& p, std::string args)
{
    if (!args.empty())
    {
        ObjectClass * obj;
        // is it in the room?
        if ((obj = p.FindObject(args)) != NULL)
        {
            if (obj->IsWearable()) {
                // Equip Object in obj->equippos (bitvectors?)
                p.AddInv(obj);
                // Remove from player's inventory
                p.RemFromPlayer(obj);
                p << "Equiped " << obj->GetName() << NEWLINE;
            } else {
                p << "Unable to equip" << NEWLINE;
            }
            return 1;
        }
        p << "Equip what?" << NEWLINE;
        return 1;
    } else { 
        p << "Currently Wearing:" << NEWLINE;
        if (p.GetEquiped().empty()) {
            p << "Nothing" << NEWLINE;
            return 1;
        }
        object_vector::const_iterator i_obj = p.GetEquiped().begin();
        ObjectClass * obj;
        for( ; i_obj != p.GetEquiped().end(); ++i_obj) {
            obj = (*i_obj);
            p << "[" << wearlocs[obj->GetWearLoc()] << "] " << obj->GetName() << NEWLINE;
        }
    }
    return 1;
}

int cmd_remove(Player& p, std::string args)
{
    if (!args.empty())
    {
        ObjectClass * obj;
        // is it in the room?
        if ((obj = p.FindObject(args)) != NULL)
        {
            if (obj->IsWearable()) {
                // Equip Object in obj->equippos (bitvectors?)
                p.AddInv(obj);
                // Remove from player's inventory
                p.RemFromPlayer(obj);
                p << "Equiped " << obj->GetName() << NEWLINE;
            } else {
                p << "Unable to equip" << NEWLINE;
            }
            return 1;
        }
        p << "Equip what?" << NEWLINE;
        return 1;
    } else { 
        p << "Currently Wearing:" << NEWLINE;
        if (p.GetEquiped().empty()) {
            p << "Nothing" << NEWLINE;
            return 1;
        }
        object_vector::const_iterator i_obj = p.GetEquiped().begin();
        ObjectClass * obj;
        for( ; i_obj != p.GetEquiped().end(); ++i_obj) {
            obj = (*i_obj);
            p << "[" << wearlocs[obj->GetWearLoc()] << "] " << obj->GetName() << NEWLINE;
        }
    }
    return 1;
}
