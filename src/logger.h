/* vim: set expandtab ts=4 sw=4:
 *
 * HARRY POTTER MUD - logger.h
 * Copyright (c) 2003 Gavin Mogan <halkeye@halkeye.net>
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (version 2) as published by the
 * Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * This notice must remain unaltered.
 *
 * $Id$
 */

#ifndef _LOGGER_H_
#define _LOGGER_H_

#include <iostream>
#include <iomanip>
#include <string>

enum e_logger { LOG_STANDARD, LOG_ERROR };

class Logger
{
private:
   e_logger type;
   
public:
    Logger();
    Logger& SetType(e_logger);
    // Output to log file (returns ostream so it just continues to write that line
    std::ostream& operator<<(const std::string&);
    Logger& operator<<(e_logger);
};
#endif
