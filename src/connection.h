/* vim: set expandtab ts=4 sw=4:
 *
 * HARRY POTTER MUD - connection.cpp
 * Copyright (c) 2003 Gavin Mogan <halkeye@halkeye.net>
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (version 2) as published by the
 * Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * This notice must remain unaltered.
 *
 * $Id$ 
 */

#ifndef _CONNECTION_H_
#define _CONNECTION_H_

#include <string>
#include <sys/types.h>
#include <sys/socket.h>

class Player;

class Connection
{
private:
    void SendToPlayer(std::string message);
protected:
    int    socket;
    std::string ip;
    Player * p; /* Should put in soon */
    
public:
    std::string inputbuffer;
    // Create Connection Data
    Connection(unsigned int socketd);
    // What socket is the player connected to?
    int GetSocket(void) { return socket; }
    // What ip does the player have?
    std::string GetIp(void);
   
    void SetPlayer(Player * player) { p = player; }
    Player& GetPlayer(void) { return *p; }
    Connection& operator<<(const std::string&);
    Connection& operator<<(const char * message);
    Connection& operator<<(const unsigned char * message);
};

#endif
