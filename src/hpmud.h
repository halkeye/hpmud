/* vim: set expandtab ts=4 sw=4:
 *
 * HARRY POTTER MUD - hpmud.h
 * Copyright (c) 2003 Gavin Mogan <halkeye@halkeye.net>
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (version 2) as published by the
 * Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * This notice must remain unaltered.
 *
 * $Id$
 */

#ifndef _HPMUD_H_
#define _HPMUD_H_

#include <sys/select.h>
#include <string>
#include <list>
#include "defines.h"
#include "commands.h"


class Player;

class hpmud
{
private:
    // Are we shutdown?
    bool b_shutdown;
    // Control Socket 
    int control;

public:
    connection_map connections;
    player_map players;
    object_map objects;
    area_list areas;
    command_hash_type command_hash;
    fd_set master_fdset;    
    
    static const unsigned char echo_off_str[];
    static const unsigned char echo_on_str[];
    static RoomClass * testroom;

public:
    hpmud(void);
    ~hpmud(void);

    // Are we preparing to shutdown?
    inline bool IsShutdown() { return b_shutdown; }
    inline void Shutdown() { b_shutdown = true; }
    // Set Control Socket
    inline void SetControl(int socketd) { control = socketd; }

    // Code that loops repeatedly (should be fork(), but i don't know how todo shared memory yet)
    void GameLoop(void);
    // Initialze Sockets and stuff
    int Init(int port = 2100);
    // Create Game objects
    int InitGame(void);
    bool CloseSocket(Player * p);
    bool addConnection(unsigned int socketd);
    bool recv(Player * p);
    Player * FindPlayer(std::string name);
    RoomClass * FindRoom(int rnum);
    // logs to file/stdout/etc... logtype says which file or even console it goes to
    void log(std::string message);

    // Initializing Commands
    void InitCommands(void);
    void InitObjs(void);
    void InitDB(void);

    int UpdateHandler(void);
};

extern hpmud game;

#endif
